import React, { useState } from 'react';
import QuestionComponent from './QuestionComponent';
import {questionsMapping} from '../data/mapping'; // Adjust the import path as necessar
import '../assets/style.css';
import { useNavigate } from 'react-router-dom';
import NODE_URI from '../config';


const Survey = () => {
  const navigate = useNavigate();
  // State to track user responses, structured as { questionId: answerIndex }
  const [responses, setResponses] = useState({});
  const [results, setResults] = useState(null);

  // Handle answer selection
  const onAnswerSelected = (questionId, selectedOptionIndex) => {
    setResponses({
      ...responses,
      [questionId]: selectedOptionIndex,
    });
  };

  // Handle survey submission
  const handleSubmit = async () => {
    try {
      const response = await fetch(`${NODE_URI}/submit`, {
        method: 'POST',
        mode:'cors',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            responses: Object.entries(responses).map(([questionId, answer]) => {
                const question = questionsMapping.find(q => q.id === parseInt(questionId));
                return {
                    questionId: parseInt(questionId),
                    answer,
                    grayIndices: question ? question.gray : []
                  };
        }),
      }),
    });
  
      if (response.ok) {
        const result = await response.json();
        console.log('Submission successful', result);
        // Handle successful submission (e.g., show a thank-you message)
        setResults(result.survey);
      } else {
        // Handle server errors or invalid responses
        console.error('Submission failed', response.status, response.statusText);
      }
    } catch (error) {
      console.error('Error submitting survey', error);
    }
  };
  const handleButtonClick = () => {
    navigate('/what-next');
  };

  if (results) {
    return (
      <div className="survey-results">
        <h2>Wynik Testu</h2>
  
        <p>Część A: {results.graya}/6</p>
        
        <p>Część B: {results.grayb}/12</p>
        {results.graya >= 4 
          ? <p>Spełniasz kryteria do dalszej diagnostyki w kierunku ADHD</p> 
          : <p>Nie spełniasz kryteriów do dalszej diagnostyki w kierunku ADHD.</p>
        }
       
       <div className="final-button-container">

       {results.graya >= 4 
        ? <button className="submit-button" onClick={handleButtonClick}>Co dalej?</button>
        : <p>Ale jeśli Twoja głowa potrzebuje radykalnego odpoczynku, wpadnij na <a href="https://breathwork.pl">Oddychanie Neurodynamiczne Online</a></p>
      }
      </div>
      </div>
    );
  }

  return (
    <div className="survey">
   
      {questionsMapping.map((question) => (
        <QuestionComponent
          key={question.id}
          question={question}
          onAnswerSelected={(selectedOptionIndex) => onAnswerSelected(question.id, selectedOptionIndex)}
          grayIndices={question.gray}
        />
      ))}
      <div className="final-button-container">
      <button className="submit-button" onClick={handleSubmit}>Zobacz wyniki</button>
      </div>
    </div>
  );
};

export default Survey;
