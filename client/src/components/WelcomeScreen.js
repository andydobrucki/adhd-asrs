import React from 'react';
import { useNavigate } from 'react-router-dom';

const WelcomeScreen = ({}) => {
    const navigate = useNavigate();


  const startSurvey = () => {
    navigate('/survey');
  };
  return (
    
    <div>
      <h1>Witamy w Skali Samooceny ADHD (ASRS-v1.1)</h1>
      <p>Ta ankieta pomoże Ci zrozumieć, czy wykazujesz objawy ADHD.</p>
      <button className="submit-button" onClick={startSurvey}>Rozpocznij test</button>
    </div>
  );
};

export default WelcomeScreen;