// WhatNextScreen.js
import React from 'react';
import { Link } from 'react-router-dom';

const WhatNextScreen = () => {
  const items = [
    { text: 'Kontynuuj Samodiagnozowanie, dołącz do jednej z grup ADHD np tu:', link: 'https://www.facebook.com/groups/289335903104118' , linkText:'ADHD u Dorosłych - Warszawa i nie tylko'},
    { text: 'Uzyskaj opinię psychologiczną, np tu:', link: 'https://www.facebook.com/attentio.stowarzyszenie', linkText: 'Fundacja Attentio' },
    { text: 'Twoja głowa potrzebuje RADYKALNEGO odpoczynku? Wpadnij na:', link: 'https://breathwork.pl', linkText: 'Oddychanie Neurodynamiczne Online'}
    // Add more items as needed
  ];

  return (
    <div>
      {items.map((item, index) => (
        <div key={index}>
        <p>{item.text}</p>
        <Link to={item.link}>{item.linkText}</Link>
        </div>
      ))}
    </div>
  );
};

export default WhatNextScreen;