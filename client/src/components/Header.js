import React from 'react';
import { useLocation } from 'react-router-dom';
import logo from '../assets/images/coliber_use.png'; // Ensure this path is correct

function Header() {
  const location = useLocation();

  if (location.pathname === '/survey') {
    return null;
  }

  return (
    <header className="App-header">
      <img src={logo} alt="Logo" />
    </header>
  );
}

export default Header;