import React, { useState } from 'react';
import {options} from '../data/mapping'; // Ensure this import matches the file structure
import '../assets/style.css';

const QuestionComponent = ({ question, onAnswerSelected }) => {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleOptionChange = (event) => {
    const selectedIdx = parseInt(event.target.value);
    setSelectedOption(selectedIdx);
    onAnswerSelected(selectedIdx+1);

  };
return (
<div className="question">
  <p>{question.text}</p>
  <div className="options-container">
    {options.map((option, index) => (
      <div key={index} className="option">
        <label className={selectedOption === index ? 'option-label selected' : 'option-label'}>
          <input
            type="radio"
            name={`question_${question.id}`}
            value={index}
            checked={selectedOption === index}
            onChange={handleOptionChange}
            className="hidden-radio" // Add this line
            
          />
           
          {option.text}
        </label>
      </div>
    ))}
  </div>
</div>
)
    };

export default QuestionComponent;
