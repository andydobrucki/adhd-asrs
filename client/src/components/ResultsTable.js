import React, { useState, useEffect } from 'react';
import NODE_URI from '../config';

function ResultsTable() {
  const [results, setResults] = useState([]);
  const [totalSurveys, setTotalSurveys] = useState(0); // Add this line

  useEffect(() => {
    fetch(`${NODE_URI}/results`)
      .then(response => response.json())
      .then(data => {
        setResults(data.results); // Modify this line
        setTotalSurveys(data.totalSurveys); // Add this line
      })
      .catch(error => console.error('Error:', error));
  }, []);

  return (
    <div>
      <p>Total surveys: {totalSurveys}</p> {/* Add this line */}
      <table>
        <thead>
          <tr>
            <th>Created</th>
            <th>Gray A Score</th>
            <th>Gray B Score</th>
            <th>Verdict</th>
          </tr>
        </thead>
        <tbody>
          {results && results.map((result, index) => (
            <tr key={index}>
              <td>{new Date(result.time).toLocaleString()}</td>
              <td>{result.grayAScore}</td>
              <td>{result.grayBScore}</td>
              <td style={{ color: result.verdict === 'Positive' ? 'green' : 'red' }}>
                {result.verdict}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ResultsTable;