// config.js
let NODE_URI;
if(process.env.NODE_ENV === 'production') {
  NODE_URI = 'https://adhd.breathwork.pl/api/survey';
} else {
  NODE_URI = 'http://localhost:3005/api/survey';
}

export default NODE_URI;