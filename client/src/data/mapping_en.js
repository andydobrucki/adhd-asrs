const questionsMapping = [
    // Part A
    { id: 1, text: "How often do you have trouble wrapping up the final details of a project once the challenging parts have been done?", gray: [3, 4, 5] },
    { id: 2, text: "How often do you have difficulty getting things in order when you have to do a task that requires organization?", gray: [3, 4, 5] },
    { id: 3, text: "How often do you have problems remembering appointments or obligations?", gray: [3, 4, 5] },
    { id: 4, text: "How often do you avoid or delay getting started on a task that requires a lot of thought?", gray: [4,5] },
    { id: 5, text: "How often do you fidget or squirm with your hands or feet when you have to sit down for a long time?", gray: [4, 5] },
    { id: 6, text: "How often do you feel overly active and compelled to do things, as if you were driven by a motor?", gray: [4, 5] },

    // Part B
    { id: 7, text: "How often do you make careless mistakes when you have to work on a boring or difficult project?", gray: [4, 5] },
    { id: 8, text: "How often do you have difficulty keeping your attention when you are doing boring or repetitive work?", gray: [4, 5] },
    { id: 9, text: "How often do you have difficulty concentrating on what people say to you, even when they are speaking to you directly?", gray: [3, 4, 5 ] },
    { id: 10, text: "How often do you misplace or have difficulty finding things at home or at work?", gray: [4,5] },
    { id: 11, text: "How often are you distracted by activity or noise around you?", gray: [4, 5] },
    { id: 12, text: "How often do you leave your seat in meetings or other situations in which you are expected to remain seated?", gray: [3, 4, 5] },
    { id: 13, text: "How often do you feel restless or fidgety?", gray: [4, 5] },
    { id: 14, text: "How often do you have difficulty unwinding and relaxing when you have time to yourself?", gray: [4, 5] },
    { id: 15, text: "How often do you find yourself talking too much when you are in social situations?", gray: [4, 5] },
    { id: 16, text: "When you're in a conversation, how often do you find yourself finishing the sentences of the people you are talking to, before they can finish them themselves?", gray: [3, 4, 5] },
    { id: 17, text: "How often do you have difficulty waiting your turn in situations when turn taking is required?", gray: [4, 5] },
    { id: 18, text: "How often do you interrupt others when they are busy?", gray: [3, 4, 5] }
]


const options = [
    { id: 1, text: "Never" },
    { id: 2, text: "Rarely" },
    { id: 3, text: "Sometimes" },
    { id: 4, text: "Often" },
    { id: 5, text: "Very Often" }
]

module.exports = {questionsMapping, options}