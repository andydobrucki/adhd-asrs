const questionsMapping = [
    // Część A
    { id: 1, text: "Jak często masz trudności z dokończeniem finalnych zadań, gdy najtrudniejsze części zostały już wykonane?", gray: [3, 4, 5] },
    { id: 2, text: "Jak często masz trudności, kiedy musisz wykonać zadanie wymagające organizacji?", gray: [3, 4, 5] },
    { id: 3, text: "Jak często masz problemy z pamiętaniem o spotkaniach lub zobowiązaniach?", gray: [3, 4, 5] },
    { id: 4, text: "Jak często unikasz lub opóźniasz rozpoczęcie zadania, które wymaga dużej ilości myślenia?", gray: [4, 5] },
    { id: 5, text: "Jak często wiercisz się lub machasz rękami lub nogami, gdy musisz siedzieć przez dłuższy czas?", gray: [4, 5] },
    { id: 6, text: "Jak często czujesz się nadmiernie aktywny i zmuszony do robienia rzeczy, jakbyś był napędzany silnikiem?", gray: [4, 5] },

    // Część B
    { id: 7, text: "Jak często robisz nieostrożne błędy, gdy musisz pracować nad nudnym lub trudnym projektem?", gray: [4, 5] },
    { id: 8, text: "Jak często masz trudności z utrzymaniem uwagi, gdy wykonujesz nudną lub powtarzalną pracę?", gray: [4, 5] },
    { id: 9, text: "Jak często masz trudności z koncentracją nad tym, co ludzie do ciebie mówią, nawet kiedy mówią to bezpośrednio?", gray: [3, 4, 5] },
    { id: 10, text: "Jak często gubisz rzeczy lub masz trudności z ich znalezieniem w domu lub w pracy?", gray: [4, 5] },
    { id: 11, text: "Jak często jesteś rozpraszany przez aktywność lub hałas dookoła ciebie?", gray: [4, 5] },
    { id: 12, text: "Jak często wstajesz ze swojego miejsca na spotkaniach lub w innych sytuacjach, w których oczekuje się, że pozostaniesz na miejscu?", gray: [3, 4, 5] },
    { id: 13, text: "Jak często czujesz się niespokojny lub nerwowy?", gray: [4, 5] },
    { id: 14, text: "Jak często masz trudności z odprężeniem się i relaksem, gdy masz czas dla siebie?", gray: [4, 5] },
    { id: 15, text: "Jak często zdarza ci się mówić za dużo, gdy znajdujesz się w sytuacjach towarzyskich?", gray: [4, 5] },
    { id: 16, text: "Kiedy rozmawiasz, jak często zdarza ci się kończyć zdania osób, z którymi rozmawiasz, zanim sami je skończą?", gray: [3, 4, 5] },
    { id: 17, text: "Jak często masz trudności z czekaniem na swoją kolej w sytuacjach, gdy wymagane jest zachowanie kolejności?", gray: [4, 5] },
    { id: 18, text: "Jak często przerywasz innym, kiedy są zajęci?", gray: [3, 4, 5] }
]

const options = [
    { id: 1, text: "Nigdy" },
    { id: 2, text: "Rzadko" },
    { id: 3, text: "Czasami" },
    { id: 4, text: "Często" },
    { id: 5, text: "Bardzo często" }
]

module.exports = {questionsMapping, options}
