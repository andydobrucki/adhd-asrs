import React from 'react';
import { BrowserRouter as Router, Routes, Route,} from 'react-router-dom';
import Survey from './components/Survey';
import WhatNextScreen from './components/WhatNextScreen'; // Ensure this path is correct
import WelcomeScreen from './components/WelcomeScreen';
import './App.css';
import './assets/style.css';
import ResultsTable from './components/ResultsTable'; // Ensure this path is correct
import Header from './components/Header'; // Import the Header component


function App() {

  return (
    <div className="App">

        <Router>
    <Header />
          <Routes>
          <Route path="/" element={<WelcomeScreen />} />
            <Route path="/survey" element={<Survey />} />
            <Route path="/what-next" element={<WhatNextScreen />} />
            <Route path="/results" element={<ResultsTable />} />
            
          </Routes>
        </Router>
   
    </div>
  );
}

export default App;