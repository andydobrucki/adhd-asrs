import express from 'express';
import path from 'path';

import dotenv from 'dotenv';
import surveyRoutes from './routes/surveyRoutes.js'; // Adjust the path as needed
import mongoose from 'mongoose';
const app = express();
app.options('*', cors()) 
app.use(cors());

const port = process.env.PORT || 3005;
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import cors from 'cors';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

dotenv.config();

app.use(express.json()); // for parsing application/json
app.use('/api/survey', surveyRoutes);

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('MongoDB connected...'))
  .catch(err => console.log(err));

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));



// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});