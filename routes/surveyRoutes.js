// routes/surveyRoutes.js
import express from 'express';
import Survey from '../models/Survey.js'; // Path to your Survey model

const router = express.Router();

router.post('/submit', async (req, res) => {
  console.log("Attempting to submit survey")
  try {
    
    const { responses } = req.body;

    // Ensure 'responses' is an array of { questionId: Number, answer: Number }
    const { grayA, grayB }  = calculateGrayResponses(responses);
    console.log(grayA,grayB)
    const survey = new Survey({
      responses,
      graya:grayA, // Total count of gray responses
      grayb:grayB
    });

    console.log(survey.createdDate); // Add this line
    await survey.save();
    res.status(201).send({ message: "Survey submitted successfully", survey });
  } catch (error) {
    res.status(400).send({ message: "Error submitting survey", error });
  }
});



router.get('/results', async (req, res) => {
  try {
    const surveys = await Survey.find({}, 'createdDate graya grayb'); // Fetch only the createdAt, graya, and grayb fields

    console.log('Fetched surveys:', surveys); // Log the fetched surveys

    // Sort by newest
    surveys.sort((a, b) => b.createdDate - a.createdDate);

    const results = surveys.map(survey => {
      console.log('Mapping survey:', survey); // Log the survey being mapped
      return {
        time: survey.createdDate,
        grayAScore: survey.graya,
        grayBScore: survey.grayb,
        verdict: survey.graya >= 4 ? 'Positive' : 'Negative' // Replace this condition with your own logic
      };
    });

    console.log('Mapped results:', results); // Log the mapped results

    const totalSurveys = await Survey.countDocuments({});

    res.json({ results, totalSurveys });
  } catch (error) {
    res.status(500).send({ message: 'Error fetching survey results', error });
  }
});



function calculateGrayResponses(responses) {
  const responses1to6 = responses.filter(response => response.questionId >= 1 && response.questionId <= 6);
  console.log(responses1to6)
  const responses7to18 = responses.filter(response => response.questionId >= 7 && response.questionId <= 18);
  const totalGray1to6 = responses1to6
    .filter(response => response.grayIndices.includes(response.answer))
    .reduce((count, response) => count + 1, 0);

  const totalGray7to18 = responses7to18
    .filter(response => response.grayIndices.includes(response.answer))
    .reduce((count, response) => count + 1, 0);
 
  return {
    grayA:totalGray1to6,
    grayB:totalGray7to18
  };
}

export default router;
