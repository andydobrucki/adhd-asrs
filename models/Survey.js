import mongoose from 'mongoose';

const SurveySchema = new mongoose.Schema({
  createdDate: {
    type: Date,
    default: Date.now
  },
  responses: [{
    question: String,
    answer: Number, // Store the index of the selected scale option
  }],
  score: Number, 
  graya: Number,
  grayb: Number // Number of gray responses
  
});

const Survey = mongoose.model('Survey', SurveySchema);

export default Survey;